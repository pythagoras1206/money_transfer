﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class TransactionController : Controller
    {
        ApplicationContext ap;
        public TransactionController(ApplicationContext ap)
        {
            this.ap = ap;
        }
        public IActionResult Index()
        {
            User user = ap.Users.FirstOrDefault(u => u.UserName == HttpContext.User.Identity.Name);
            return View(user);
        }

        [HttpPost]
        public IActionResult Send(string getterCode, decimal amount)
        {
            User sender = ap.Users.Include(u => u.Transactions).FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            User getter = ap.Users.Include(u => u.Transactions).FirstOrDefault(u => u.Code == getterCode);
            if(getter != null && amount <= sender.Balance)
            { 
                sender.Balance -= amount;
                getter.Balance += amount;
                Transaction tr = new Transaction() { From = sender.Code, To = getter.Code, Amount = amount, Time = DateTime.Now };
                ap.Transactions.Add(tr);
                ap.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Add(decimal amount)
        {
            User user = ap.Users.Include(u => u.Transactions).FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            user.Balance += amount;
            ap.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Story(SortState sortOrder = SortState.DateAsc)
        {
            User user = ap.Users.Include(u => u.Transactions).FirstOrDefault(u => u.UserName == HttpContext.User.Identity.Name);
            IQueryable<Transaction> transactions = ap.Transactions.Where(u => u.From == user.Code || u.To == user.Code);

            ViewBag.DateSort = sortOrder == SortState.DateDesc ? SortState.DateAsc : SortState.DateDesc;
            switch (sortOrder)
            {
                case SortState.DateAsc:
                    transactions.OrderBy(u => u.Time);
                    break;
                case SortState.DateDesc:
                    transactions.OrderByDescending(u => u.Time);
                    break;
            }
            return View(transactions);
        }
    }
    
}