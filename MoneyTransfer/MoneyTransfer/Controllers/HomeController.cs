﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class HomeController : Controller
    {
        public ApplicationContext ap;
        public HomeController(ApplicationContext ap)
        {
            this.ap = ap;
        }
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult Add(string code, decimal amount)
        {
            User user = ap.Users.FirstOrDefault(u => u.Code == code);
            if (user != null)
            {
                user.Balance += amount;
                Transaction tr = new Transaction() { From = user.Code, To = user.Code, Amount = amount, Time = DateTime.Now };
                ap.Transactions.Add(tr);
                ap.SaveChanges();
            }

            return RedirectToAction("Index");
            
        }
    }
}
