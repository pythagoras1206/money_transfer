﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        public string Code { get; set; }
        public decimal Balance { get; set; }

        public int TransactionId { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}
